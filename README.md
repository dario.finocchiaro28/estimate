# Estimates

This project provides API built on top of Lumen.

The web server (apache) and the database (mariadb) run on docker containers.

## Setup

You should copy the `.env.example` as `.env` and set the appropriate configurations. In particular:

```yaml
DB_HOST=db
DB_PORT=3306
DB_DATABASE=estimate
DB_USERNAME=root
DB_PASSWORD=root
MYSQL_DATABASE=estimate
MYSQL_USER=root
MYSQL_PASSWORD=root
MYSQL_ROOT_PASSWORD=root
```

You should create a `docker-compose.override.yml` to setup networks configurations for the containers (port binding):
```yaml
version: "3.5"
services:
  web:
    ports:
      - "8081:80"
  db:
    ports:
      - "33006:3306"
```

Run 

```
composer install
```


## Run

A Makefile is provided to enable shortcuts:
- `make run` starts docker containers
- `make stop` stops docker containers
- `make migrations` executes db migrations

- Run `make run` to startup the system

- Create a database on the container (the name must match the one set in the .env) and run

```
make migrations
```

If you get an error while running migrations, change in you .env file the db configuration to bind local (host = 127.0.0.1 and the port = the port you set in the docker-compose.override).
After executing migrations reset values, otherwise the web app wont work

- Go to `localhost:8081/public` (port should match the one you configured in the docker-compose.override)

If you get error of Permission denied for log files, ensure that `storage/logs/` has 777 permission

## API

To see the available endpoints run

```
make list_routes
```
