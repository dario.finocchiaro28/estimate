#!make

help: ## Show command list
	@awk -F ':|##' '/^[^\t].+?:.*?##/ {printf "\033[36m%-30s\033[0m %s\n", $$1, $$NF}' $(MAKEFILE_LIST)

run:
	docker-compose up -d --remove-orphans

stop:
	docker-compose stop

migrations:
	php artisan migrate

list_routes:
	php artisan route:list
