<?php

declare(strict_types=1);

namespace App\Model;

interface EstimateInterface
{
    public function getId(): int;

    public function getCustomer(): string;

    public function getPrice(): int;

    public function getNotes(): string;
}
