<?php

declare(strict_types=1);

namespace App\Model;

class Estimate extends BaseModel implements EstimateInterface
{
    private const ID = 'id';
    private const CUSTOMER = 'customer';
    private const PRICE = 'price';
    private const NOTES = 'notes';

    private $id;

    private $customer;

    private $price;

    private $notes;

    public function __construct(
        int $id,
        string $customer,
        int $price,
        string $notes
    ) {
        $this->id = $id;
        $this->customer = $customer;
        $this->price = $price;
        $this->notes = $notes;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCustomer(): string
    {
        return $this->customer;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getNotes(): string
    {
        return $this->notes;
    }

    public function toJson($options = 0): string
    {
        return json_encode($this, $options);
    }

    public function jsonSerialize()
    {
        return [
            self::ID       => $this->id,
            self::CUSTOMER => $this->customer,
            self::PRICE    => $this->price,
            self::NOTES    => $this->notes
        ];
    }
}
