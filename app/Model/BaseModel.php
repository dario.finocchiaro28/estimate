<?php

declare(strict_types=1);

namespace App\Model;

use App\Exceptions\UnimplementedException;
use Illuminate\Contracts\Support\Jsonable;
use JsonSerializable;

abstract class BaseModel implements Jsonable, JsonSerializable
{
    public function toJson($options = 0): string
    {
        throw new UnimplementedException('Missing concrete implementation for toJson method');
    }

    public function jsonSerialize()
    {
        throw new UnimplementedException('Missing concrete implementation for jsonSerialize method');
    }
}
