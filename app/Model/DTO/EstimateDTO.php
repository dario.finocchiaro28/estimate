<?php

declare(strict_types=1);

namespace App\Model\DTO;

class EstimateDTO
{
    private $customer;

    private $price;

    private $notes;

    public function __construct(
        string $customer,
        int $price,
        string $notes
    ) {
        $this->customer = $customer;
        $this->price = $price;
        $this->notes = $notes;
    }

    public function getCustomer(): string
    {
        return $this->customer;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getNotes(): string
    {
        return $this->notes;
    }
}
