<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Exceptions\ApiHandler;
use App\Model\DTO\EstimateDTO;
use App\Service\EstimateService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

//TODO add APIDocs
//TODO authentication
//TODO validation
class EstimateController extends Controller
{
    private $estimateService;

    private $exceptionHandler;

    public function __construct(
        EstimateService $estimateService,
        ApiHandler $exceptionHandler
    ) {
        $this->estimateService = $estimateService;
        $this->exceptionHandler = $exceptionHandler;
    }

    public function all(): JsonResponse
    {
        $estimates = $this->estimateService->getAll();

        return new JsonResponse($estimates->toArray());
    }

    /**
     * @throws Exception
     */
    public function one(int $id): JsonResponse
    {
        try {
            $estimate = $this->estimateService->getOneById($id);
            return new JsonResponse($estimate);
        } catch (Exception $exception) {
            return $this->exceptionHandler->handle($exception);
        }
    }

    public function new(Request $request): JsonResponse
    {
        $customer = $request->get('customer');
        $price = (int) $request->get('price');
        $notes = $request->get('notes');

        $estimateDto = new EstimateDTO($customer, $price, $notes);

        return new JsonResponse($this->estimateService->createNew($estimateDto),
            Response::HTTP_CREATED);
    }

    /**
     * @throws Exception
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $customer = $request->get('customer');
        $price = (int) $request->get('price');
        $notes = $request->get('notes');

        $estimateDto = new EstimateDTO($customer, $price, $notes);

        try {
            $result = $this->estimateService->update($id, $estimateDto);
            return new JsonResponse($result);
        } catch (Exception $exception) {
            return $this->exceptionHandler->handle($exception);
        }
    }

    /**
     * @throws Exception
     */
    public function delete(int $id): JsonResponse
    {
        try {
            $estimate = $this->estimateService->delete($id);
            return new JsonResponse($estimate);
        } catch (Exception $exception) {
            return $this->exceptionHandler->handle($exception);
        }
    }
}
