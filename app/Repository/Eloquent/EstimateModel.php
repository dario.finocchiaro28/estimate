<?php

declare(strict_types=1);

namespace App\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;

class EstimateModel extends Model
{
    protected $table = 'estimates';

    protected $fillable = ['customer', 'price', 'notes'];
}
