<?php

declare(strict_types=1);

namespace App\Repository\Eloquent;

use App\Exceptions\DeleteFailedException;
use App\Exceptions\EntityNotFoundException;
use App\Exceptions\UpdateFailedException;
use App\Model\DTO\EstimateDTO;
use App\Model\Estimate;
use App\Model\EstimateInterface;
use App\Repository\EstimateRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class EstimateRepository extends BaseRepository
    implements EstimateRepositoryInterface
{
    private const ID = 'id';
    private const CUSTOMER = 'customer';
    private const PRICE = 'price';
    private const NOTES = 'notes';

    public function __construct(EstimateModel $model)
    {
        parent::__construct($model);
    }

    private function recordMapper(Model $record): EstimateInterface
    {
        $id = $record->getAttributeValue(self::ID);
        $customer = $record->getAttributeValue(self::CUSTOMER);
        $price = $record->getAttributeValue(self::PRICE);
        $notes = $record->getAttributeValue(self::NOTES);

        return new Estimate($id, $customer, $price, $notes);
    }

    private function entityMapper(EstimateDTO $estimate): array
    {
        return [
            self::CUSTOMER => $estimate->getCustomer(),
            self::PRICE    => $estimate->getPrice(),
            self::NOTES    => $estimate->getNotes()
        ];
    }

    public function fetchAll(): Collection
    {
        $records = parent::all();

        $collection = $records->map(fn($item) => $this->recordMapper($item));

        return $collection;
    }

    /**
     * @throws EntityNotFoundException
     */
    public function findOneById(int $id): EstimateInterface
    {
        $record = parent::find($id);

        if ($record === null) {
            throw new EntityNotFoundException();
        }

        return $this->recordMapper($record);
    }

    public function add(EstimateDTO $estimate): EstimateInterface
    {
        $record = parent::create($this->entityMapper($estimate));

        return $this->recordMapper($record);
    }

    /**
     * @throws EntityNotFoundException
     * @throws UpdateFailedException
     */
    public function updateById(int $id, EstimateDTO $estimate): EstimateInterface
    {
        $record = parent::find($id);

        if ($record === null) {
            throw new EntityNotFoundException();
        }

        if (!$record->update($this->entityMapper($estimate))) {
            throw new UpdateFailedException();
        }

        return $this->recordMapper($record);
    }

    /**
     * @throws EntityNotFoundException
     * @throws DeleteFailedException
     */
    public function deleteById(int $id): EstimateInterface
    {
        $record = parent::find($id);

        if ($record === null) {
            throw new EntityNotFoundException();
        }

        if (!$record->delete()) {
            throw new DeleteFailedException();
        }

        return $this->recordMapper($record);
    }
}
