<?php

declare(strict_types=1);

namespace App\Repository\Eloquent;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface EloquentRepositoryInterface
{
    public function all(): Collection;

    public function find($id): ?Model;

    public function create(array $attributes): Model;
}
