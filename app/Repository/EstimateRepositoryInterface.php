<?php

declare(strict_types=1);

namespace App\Repository;

use App\Model\DTO\EstimateDTO;
use App\Model\EstimateInterface;
use Illuminate\Support\Collection;

interface EstimateRepositoryInterface
{
    public function fetchAll(): Collection;

    public function findOneById(int $id): EstimateInterface;

    public function add(EstimateDTO $estimate): EstimateInterface;

    public function updateById(int $id, EstimateDTO $estimate): EstimateInterface;

    public function deleteById(int $id): EstimateInterface;
}
