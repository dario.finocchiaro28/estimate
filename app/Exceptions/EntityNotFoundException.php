<?php

declare(strict_types=1);

namespace App\Exceptions;

use Throwable;

class EntityNotFoundException extends ApiException
{
    public function __construct(
        $message = "Entity not found",
        $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
