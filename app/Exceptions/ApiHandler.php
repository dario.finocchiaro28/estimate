<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ApiHandler
{
    /** @var string[] */
    private $handled
        = [
            EntityNotFoundException::class,
            UpdateFailedException::class,
            DeleteFailedException::class,
        ];

    /**
     * @throws Exception
     */
    public function handle(Exception $exception): JsonResponse
    {
        if (!$this->canHandle($exception)) {
            throw $exception;
        }

        $statusCode = $this->determineStatusCode($exception);

        return new JsonResponse($exception, $statusCode);
    }

    public function canHandle(Exception $exception): bool
    {
        return in_array(get_class($exception), $this->handled);
    }

    private function determineStatusCode(Exception $exception): int
    {
        switch (get_class($exception)) {
            case EntityNotFoundException::class:
                return Response::HTTP_NOT_FOUND;
            case UpdateFailedException::class:
            case DeleteFailedException::class:
                return Response::HTTP_UNPROCESSABLE_ENTITY;
            default:
                return Response::HTTP_INTERNAL_SERVER_ERROR;
        }
    }
}
