<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;
use Illuminate\Contracts\Support\Jsonable;
use JsonSerializable;

abstract class ApiException extends Exception
    implements Jsonable, JsonSerializable
{
    public function toJson($options = 0): string
    {
        return json_encode($this->jsonSerialize());
    }

    public function jsonSerialize()
    {
        return [
            "message" => $this->message,
        ];
    }
}
