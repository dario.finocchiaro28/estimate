<?php

declare(strict_types=1);

namespace App\Exceptions;

use Throwable;

class UpdateFailedException extends ApiException
{
    public function __construct(
        $message = "Update failed. Please retry",
        $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
