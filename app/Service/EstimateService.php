<?php

declare(strict_types=1);

namespace App\Service;

use App\Model\DTO\EstimateDTO;
use App\Model\EstimateInterface;
use App\Repository\EstimateRepositoryInterface;
use Illuminate\Support\Collection;

class EstimateService
{
    private $estimateRepository;

    public function __construct(EstimateRepositoryInterface $estimateRepository)
    {
        $this->estimateRepository = $estimateRepository;
    }

    public function getAll(): Collection
    {
        return $this->estimateRepository->fetchAll();
    }

    public function getOneById(int $id): EstimateInterface
    {
        return $this->estimateRepository->findOneById($id);
    }

    public function createNew(EstimateDTO $estimate): EstimateInterface
    {
        return $this->estimateRepository->add($estimate);
    }

    public function update(int $id, EstimateDTO $estimate): EstimateInterface
    {
        return $this->estimateRepository->updateById($id, $estimate);
    }

    public function delete(int $id): EstimateInterface
    {
        return $this->estimateRepository->deleteById($id);
    }
}
