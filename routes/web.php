<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('/estimates',  ['uses' => 'EstimateController@all']);

    $router->get('/estimates/{id}', ['uses' => 'EstimateController@one']);

    $router->post('/estimates', ['uses' => 'EstimateController@new']);

    $router->put('/estimates/{id}', ['uses' => 'EstimateController@update']);

    $router->delete('/estimates/{id}', ['uses' => 'EstimateController@delete']);
});
